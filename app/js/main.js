$(function(){
  $('#go1').click( function(event){
  		$('#overlay').fadeIn(400,
  		 	function(){
  				$('#modal__form')
  					.css('display', 'block')
  					.animate({opacity: 1, top: '50%'}, 200);
  		});
  	});
  $('#go2').click( function(event){
  		$('#overlay').fadeIn(400,
  		 	function(){
  				$('#modal__form')
  					.css('display', 'block')
  					.animate({opacity: 1, top: '50%'}, 200);
  		});
  	});
  $('#go3').click( function(event){
  		$('#overlay').fadeIn(400,
  		 	function(){
  				$('#modal__form')
  					.css('display', 'block')
  					.animate({opacity: 1, top: '50%'}, 200);
  		});
  	});
  $('#go4').click( function(event){
  		$('#overlay').fadeIn(400,
  		 	function(){
  				$('#modal__form')
  					.css('display', 'block')
  					.animate({opacity: 1, top: '50%'}, 200);
  		});
  	});
  $('#modal__close, #overlay').click( function(){ // лoвим клик пo крестику или пoдлoжке
  		$('#modal__form')
  			.animate({opacity: 0, top: '45%'}, 200,  // плaвнo меняем прoзрaчнoсть нa 0 и oднoвременнo двигaем oкнo вверх
  				function(){ // пoсле aнимaции
  					$(this).css('display', 'none'); // делaем ему display: none;
  					$('#overlay').fadeOut(400); // скрывaем пoдлoжку
  				}
  			);
  	});
  $('.slider-head').slick({
    dots: true,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true
  });
  var timerId = setInterval(function() {
    if($('#slick-slide00').hasClass('slick-current') == true) {
      $('#header').css('background-image', 'url(../img/bg-header1.png)');
    }
    if($('#slick-slide01').hasClass('slick-current') == true) {
      $('#header').css('background-image', 'url(../img/bg-header2.png)');
    }

  }, 100);
  $('#burger').click(function(){
    $('.menu--mob').toggleClass('menu-active');
    $('.main-wrap').toggleClass('blur');
    $('.main-wrap, .menu__wrapper li a').click(function () {
      $('.menu--mob').removeClass('menu-active');
      $('.main-wrap').removeClass('blur');
    });
  });
  $(".link-top").click(function () {
      elementClick = $(this).attr("href");
      destination = $(elementClick).offset().top;
      $("body,html").animate({scrollTop: destination }, 900);
  });
  ymaps.ready(init);
  function init() {
  var myMap = new ymaps.Map("map", {
          center: [55.8806708, 37.49620649999997],
          zoom: 17
      },
      $('.contact__wrapper').on('click', '.contact__label', function () {
        var type = this.getAttribute('data-type');
        console.log(type);
        switch (type) {
          case 'one':
          // Москва Бусиновская горка дом 1В
          myMap.panTo([55.8806708, 37.49620649999997], {flying: true});
            break;
          case 'two':
          //г. Мытищи Проектируемый проезд 497, возле ТЦ Июнь
          myMap.panTo([55.9197346, 37.70862850000003], {flying: true});
            break;
          case 'three':
          //г. Москва 1-я ул Измайловского зверинца 19а
          myMap.panTo([55.7779879, 37.74547089999999], {flying: true});
            break;
          case 'four':
          //г. Москва Батюнинский проезд 25
          myMap.panTo([55.6600372, 37.70666870000002], {flying: true});
            break;
          case 'five':
          //г. Москва Кавказский бульвар 52а
          myMap.panTo([55.630362, 37.64462389999994], {flying: true});
            break;
          case 'six':
          //г. Москва ул генерала Дорохова 18а
          myMap.panTo([55.6913178, 37.44337840000003], {flying: true});
            break;
        }
      }));

      // Москва Бусиновская горка дом 1В
      myGeoObject1 = new ymaps.GeoObject({
          geometry: {
              type: "Point",
              coordinates: [55.8806708, 37.49620649999997]
          }
      });
      //г. Мытищи Проектируемый проезд 497, возле ТЦ Июнь
      myGeoObject2 = new ymaps.GeoObject({
          geometry: {
              type: "Point",
              coordinates: [55.9197346, 37.70862850000003]
          }
      });
      //г. Москва 1-я ул Измайловского зверинца 19а
      myGeoObject3 = new ymaps.GeoObject({
          geometry: {
              type: "Point",
              coordinates: [55.7779879, 37.74547089999999]
          }
      });
      //г. Москва Батюнинский проезд 25
      myGeoObject4 = new ymaps.GeoObject({
          geometry: {
              type: "Point",
              coordinates: [55.6600372, 37.70666870000002]
          }
      });
      //г. Москва Кавказский бульвар 52а
      myGeoObject5 = new ymaps.GeoObject({
          geometry: {
              type: "Point",
              coordinates: [55.630362, 37.64462389999994]
          }
      });
      //г. Москва ул генерала Дорохова 18а
      myGeoObject6 = new ymaps.GeoObject({
          geometry: {
              type: "Point",
              coordinates: [55.6913178, 37.44337840000003]
          }
      });
      myMap.geoObjects.add(myGeoObject1)
                      .add(myGeoObject2)
                      .add(myGeoObject3)
                      .add(myGeoObject4)
                      .add(myGeoObject5)
                      .add(myGeoObject6);

}
});
